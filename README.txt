# IFTTT Rules

This module provides a rules action to be used to trigger IFTTT maker recipes.
Some very popular scenarios you might want to try out would be:
- Trigger a slack message when a user logs in.
- Turn on the kitchen lights when a node is being deleted.
- Keep a log of all deleted nodes in a google spreadsheet.


REQUIREMENTS

- Drupal 7.x
- Rules 7.x-2.x

INSTALLATION

- Copy the ifttt_rules directory to your modules directory.
- Enable Rules UI and start building some automation. Select an IFTTT action and
configure it.

AUTHOR AND CREDITS

Victor  Ursu - victoru - Maintainer
